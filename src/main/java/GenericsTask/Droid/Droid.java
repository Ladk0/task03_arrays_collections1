package GenericsTask.Droid;

public abstract class Droid {
    private int health;
    private int armor;
    private int damage;

    protected Droid(){
        this.health = 100;
    }

    protected int getHealth(){
        return this.health;
    }
    protected int getArmor(){
        return this.armor;
    }
    protected int getDamage(){
        return this.damage;
    }
    protected void setHealth(int health){
        this.health = health;
    }
    protected void setArmor(int armor){
        this.armor = armor;
    }
    protected void setDamage(int damage){
        this.damage = damage;
    }
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
