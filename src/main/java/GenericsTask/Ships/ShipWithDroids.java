package GenericsTask.Ships;
import java.util.ArrayList;
import java.util.List;

public class ShipWithDroids<T> {
    ArrayList<T> shipList = new ArrayList<T>();

    public T get(int i){
        return shipList.get(i);
    }
    public List<? super T> get(int first, int last){
       return shipList.subList(first, last);
    }
    public void put(T droid){
        shipList.add(droid);
    }
    public void put(List<? extends T> list){
        shipList.addAll(list);
    }
    public List<? super T> getList(){
        return shipList;
    }
    public int size(){
        return shipList.size();
    }

}
