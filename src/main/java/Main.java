import GenericsTask.Droid.ArmoredDroid;
import GenericsTask.Droid.BalancedDroid;
import GenericsTask.Droid.BattleDroid;
import GenericsTask.Droid.Droid;
import GenericsTask.Ships.ShipWithDroids;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        ShipWithDroids<Droid> ship = new ShipWithDroids<Droid>();
        ArrayList<Droid> someDroids = new ArrayList<Droid>();

        someDroids.add(new BalancedDroid());
        someDroids.add(new BattleDroid());
        someDroids.add(new ArmoredDroid());
        ship.put(someDroids);

        ship.put(new BattleDroid());
        ship.put(new ArmoredDroid());


        for(int i = 0; i<ship.size(); i++){
            System.out.println(ship.get(i).toString());
        }
    }
}
